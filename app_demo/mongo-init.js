db.auth('admin', 'mongo')

db = db.getSiblingDB('demodb')

db.createUser(
    {
        user: "demodb",
        pwd: "demodb",
        roles: [
            {
                role: "readWrite",
                db: "demodb"
            }
        ]
    }
);