# TP Kubernetes

Dans ce TP nous allons déployer une application Nodejs/MongoDB, associé qu'une supervision via Prometheus/Grafana.

L'Application alimente la base de donnée MongoDB, avec des messages personnalisé par l'utilisateur, ainsin que des données alléatoires.

Prometheus serra configuré pour intéroger l'Application, va récupérer le status de l'Application et de MongoDB (1/0 pour Up/Down), ainsin que les données aléatoires dans la base de donnée.

Grafana va afficheir les données via l'interogation du Prometheus.

## Contexte

Nous avons utilisé deux VM pour réaliser ce TP:

- Kubernetes sous Ubuntu-20.04.1 avec Microk8s client/serveur v1.3.7
- Machine de développement et cliente de Kubernetes sous Centos7 avec Docker Enjine 20.10.1

## Prérequis
Instalation de microk8s: https://microk8s.io/tutorials

Dans microk8s, s'assurer que les composants suivant soient actifs:

    microk8s enable *nom du composant*

- registry
- ingress
- dashboard
- storage
- dns

Après cela les Namespaces "ingress" et "container-registry" seront visibles. Pour vérivier:

    kubectl get ns
    NAME                 STATUS   AGE
    kube-system          Active   15d
    kube-public          Active   15d
    default              Active   15d
    kube-node-lease      Active   2d19h
    ingress              Active   151m
    container-registry   Active   147m

Pour exposer le port du dashboard à fin d'y accéder depuis l'extérieur:

    microk8s kubectl port-forward --address 0.0.0.0 -n kube-system service/kubernetes-dashboard 10443:443

Pour exposer le port de la registery kubernetes, pour la rendre accessible de l'éxterieur:

    microk8s kubectl port-forward --address 0.0.0.0 -n container-registry service/registry 5000:5000

Avant de créer l'image de notre application nous allons autoriser les connections en http entre docker et la registry en éditant ou en créant /etc/docker/daemon.json. Ajoutez:

    {  "insecure-registries" : ["IpKubernetes:5000"]}

Le build a été fait précedement et exporté via Docker, nous allons la recharger, puis pousser dans notre regsitry. Sur votre système possèdant docker engine:

    docker load < app-demo.tar
    docker tag tho/app-demo:v1 "IpKubernetes:5000/tho/app-demo:v1"
    docker push IpKubernetes:5000/tho/app-demo:v1

Vous devriez voir le push se faire (30s à 1min).

# Ingress
Les acces externes aux différents composants sont routés sur les différents services via Ingress basé sur le FQDN demandé. Dans notre configuration actuel, ceci sont utilisés:

- http://prometheus.tho.local
- http://grafana.tho.local
- http://app-demo.tho.local

Il est imporant de reseigner ces FQDN avec l'IP du Kubernetes. Exemple:

    192.168.1.177 prometheus.tho.local
    192.168.1.177 grafana.tho.local
    192.168.1.177 app-demo.tho.local



## Modifications des FQDN si besoin

Vous pouvez modifier les entrées __spec.rules.host__ dans les fichiers:

- tpk8s/prometheus/k8s/ingress.yml
- tpk8s/grafana/k8s/ingress.yml
- tpk8s/app_demo/k8s/all.yml

Exemple:

    apiVersion: extensions/v1beta1
    kind: Ingress
    ...
    spec:
        rules:
            - host: grafana.tho.local
            http:
                ...

Et adapter votre etc/host en fonction.

# Creation du namespace
Nous créons un nouveau namespace pour ce TP.

    kubectl create namespace tpk8s

# Déploiement de prometheus

    cd "YourPath"/tpk8s/prometheus/k8s

## Creation du config-map
Nous avons déjà une configuration disponible pour que notre Prometheus communique avec notre application. Nous voulons donc l'appliquer au lancement du Pod.

    kubectl apply -f config-map.yml -n tpk8s

## Déploiement de Prometheus
    kubectl apply -f deployment.yml -n tpk8s

## Creation du service
    kubectl apply -f service.yml -n tpk8s

## Application de l'ingress
    kubectl apply -f ingress.yml -n tpk8s  

# Déploiement de grafana
    cd "YourPath"/tpk8s/grafana/k8s

## Creation config-map
Nous avons déjà une configuration disponible pour que notre Grafana. Nous voulons donc l'appliquer au lancement du Pod.

    kubectl apply -f config-map.yml -n tpk8s

## Déploiement Grafana
    kubectl apply -f deployment.yml -n tpk8s

## Creation service
    kubectl apply -f service.yml -n tpk8s

## Apply of the ingress
    kubectl apply -f ingress.yml -n tpk8s

## Apply pvclaim
    kubectl apply -f pvclaim.yml -n tpk8s

Note: microk8s génère automatiquement les PV lors d'un claim.

# Déploiement de l'application et MongoDB

    cd "YourPath"/tpk8s/app_demo/k8s

## Déploiement de l'application et de la base de donnée MongoDB
Nous avons mis tout sous un seul fichier 'all.yml' pour simplifier.

    kubectl apply -f all.yml -n tpk8s

## Vérification du bon fonctionnement

Pour se connecter sur l'Application, les informations sont admin/admin.

Il peut arriver qu'une course critique puisse arriver entre l'application et la bdd. Aller dans le menu "Api > /api/status".

Si tout est OK: {"status":"ok","db":1}.

Si "db":0, attendre que la base soit bien up, puis effacer le pod applicatif (recreation automatique).

# Post configuration Grafana
Il est obligatoire de configurer Grafana pour récupérer les informations de Prometheus. utilisez l'URL http://grafana.tho.local (ou autre si vous l'avez modifé), les login sont admin/admin par défaut.

Une fois dans l'interface vous pouvez aller sur la gauche dans "Configuration" puis "Data source" et "Add data source". Vous pouvez sélectionner Pormetheus et dans "URL" vous pouvez rajouter: http://prometheus-service:9090.

Pour rajouter un dashboard, sur la gauche, dans "Create" puis "Import". Vous pouvez soit mettre en ligne le fichier "YourPath"/tpk8s/grafana/dashboard/dashboardGraf.json, ou bien copier coller sont contenue et faire "load" puis "Import".

Vous pouvez y accéder via "Dashboard" sur la gauche, puis en sélectionnant "tpk8s" dans l'onglet "Dasboards".
