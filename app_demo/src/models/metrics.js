var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var MetricSchema = new Schema({
  value: Number,
  name: String,
  date: {
    type: Date,
    default: Date.now
  }
});

module.exports = mongoose.model('Metric', MetricSchema );
