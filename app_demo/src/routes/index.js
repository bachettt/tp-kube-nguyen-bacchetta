var express = require('express');
var router = express.Router();
passport = require('passport')
var execSync = require('child_process').execSync;
const os = require('os');
const Message = require('../models/message')




const fs = require('fs');

router.get('/', (req, res) => {
  // Check if a user is logged-in, is authenticated
  if ( !req.isAuthenticated() ) {
    res.redirect('/login')
    return
  }
  res.render('index', { title: 'Demo apps', user: req.user,session:req.session});
});

router.get('/login', function(req, res, next) {
  res.render('login', { title: 'Login' ,message:req.message })
});

router.post('/login', 
  passport.authenticate('local', { failureRedirect: '/login' }),
  function(req, res) {

    res.redirect('/');
});


router.get('/logout',
  function(req, res){
    req.logout();
    res.redirect('/');
});


router.get('/accounts', function(req, res, next) {
  if ( !req.isAuthenticated() ) {
    res.redirect('/login')
    return
  }
  accounts=req.app.get("accounts")
  res.render('app_accounts', { title: 'Configuration',  user: req.user,"accounts":accounts.records});
});

router.post('/account', function(req, res, next) {
  accounts=req.app.get("accounts")
  account=req.body
  accounts.add(account,(err,userid)=>{
    res.json({err:err,userid:userid})
  })
});

router.post('/account/update', function(req, res, next) {
  accounts=req.app.get("accounts")
  account=req.body
  accounts.update(account,(err,user) =>{
    res.json({err:err,user:user})
  })
});

router.delete('/account', function(req, res, next) {
  accounts=req.app.get("accounts")
  account=req.body
  accounts.remove(account.id,(rep)=>{
    res.json(rep) 

  })
});

router.get('/signup', (req, res) => {
  res.render('signup', { title: 'Signup'});
});

router.post('/signup', function(req, res, next) {
  accounts=req.app.get("accounts")
  account=req.body
  accounts.add(account,(err,userid)=>{
    res.redirect('/');
  })
});

router.get("/console", function(req, res) {
  res.render("console", { title: 'Console',user: req.user,hostname: os.hostname, uptime: os.uptime });
});

router.post("/console", function(req, res) {
  cmd = req.body.command;
  var options = {
    encoding: 'utf-8'
  }
  res.render("console", {user: req.user,hostname: os.hostname, uptime: os.uptime , result: execSync(cmd,options)});
});


router.get("/system", function(req, res) {
  res.render("system", { title: 'Systeme',user: req.user,hostname: os.hostname, uptime: os.uptime });
});

router.get('/messages', function (req, res, next) {
  res.render('messages', { title: 'MongoDb Demo', user: req.user   });

});
router.get('/graph', function(req, res, next) {
  res.render('graph', { title: 'Graphiques' });
});


module.exports = router;
