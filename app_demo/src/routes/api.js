// Sauvegarde de la configuration
const cookieParser = require('cookie-parser');
var express = require('express');
var router = express.Router();
const os = require('os');
const Message = require('../models/message')
const Metric = require('../models/metrics')




router.use(function (req, res, next) {
    console.log(req.path)
    if( req.path == '/metrics' || req.path == '/metrics/prometheus'  ){
     next()
    } else {
      if ( !req.isAuthenticated() && !req.tokenVerified ) {

          if( !req.isAuthenticated() && req.path =='/' ){
              res.redirect('/login')
              return
          } else {
              res.json({"error":"Bad Api token"})
          }
      } else {
          next();
      }
    }
});
// Generation de la page de configuration
router.get('/', function(req, res, next) {
    res.setHeader("x-api-key",req.user.token);
    res.render('api_home', { title: 'Api' ,user: req.user});
});

router.get('/status', function(req, res, next) {
    data={'status':'ko'}
    dbcon=req.app.get('dbcon')
    data.db=dbcon.connection.readyState;
    data.status="ok"
    res.json(data);
 });


router.get('/session', function(req, res, next) {
   res.json(req.session);
});



router.get('/header', function(req, res, next) {
    res.json(req.headers);
 });
 

router.get('/infos', function(req, res, next) {
    appconfig=req.app.get('appconfig')
    res.json(appconfig);
 });

 router.get('/accounts', function(req, res, next) {
    accounts=req.app.get('accounts')
    res.json(accounts.records);
 });

 
 router.get('/account/:id', function(req, res, next) {
    var id = req.params.id;
    accounts=req.app.get('accounts')
    res.json(accounts.records[+id-1]);
 });

 router.get('/osinfo',function(req, res, next){
    var data={"hostname":os.hostname(),"plateform":os.platform(),"release":os.release(),"uptime":os.uptime(),"totalmem":os.totalmem(),"freemem":os.freemem(),"Database": process.env.MONGO_URL,"network_interfaces":os.networkInterfaces()}
    res.json(data);
  });
  
  router.get('/messages',function(req, res, next){
    let mess = new Message({content:"Get Messages", crit:"Info"})
    mess.save(function() {
      Message.find({},function(err,docs){
       res.json(docs);
      })
    });
  });
  
  router.post('/message',function(req, res, next){
    let mess = new Message({content:req.body.content, crit:req.body.crit})
    mess.save(function(){
      Message.find({},function(err,docs){
        res.json(docs);
     })
    })
  });
  
  router.delete('/messages',function(req, res, next){
    let mess = new Message({content:req.body.content, crit:req.body.crit})
    mess.save(function(){
      Message.remove({},function(err,docs){
        res.json(docs);
     });
    });
  });

router.get('/metrics', function(req, res, next) {
    Metric.find().distinct('name', function(error, docs) {
        res.json(docs);
    });
    
})
  
router.get('/metric/:name', function(req, res, next) {
  Metric.find({name:req.params.name},function(err,docs){
         res.json(docs);
      })
});

router.get('/metric/graph/:name', function(req, res, next) {
  var series=[]
  Metric.find({name:req.params.name},function(err,docs){
      for(i in docs){
             series.push({x:docs[i].date,y:docs[i].value})
         }
      res.json(series);
  })
});

router.get('/metric/:name/last', function(req, res, next) {
  Metric.findOne({name:req.params.name},{}, { sort: { 'date' : -1 } }, function(err,docs){
      res.json(docs);
  })
});


router.get('/metrics/prometheus', function(req, res, next) {
  var result=[]
  dbcon=req.app.get('dbcon')

  Metric.find().distinct('name').then( function(names) {
      var jobQueries=[];
      names.forEach(function(u){
        console.log(u)
        jobQueries.push(Metric.findOne({name:u},{}, { sort: { 'date' : -1 } }));

      });
      return Promise.all(jobQueries)
  }).then(function(datas) {
      console.log(datas)
      var d = Date.now()
      var meta_name="tuto_status"
      result+="# HELP "+meta_name+' Etat de applicactif (0=KO,1=OK)\n'
      result+="# TYPE "+meta_name+" gauge\n"
      result+=meta_name+'{status="app"} 1 '+d+'\n'
      result+=meta_name+'{status="dbstaste"} '+dbcon.connection.readyState+' '+d+'\n'
      var meta_name='demo_metric'
      result+="# HELP "+meta_name+" Aide sur la metric "+meta_name+'\n'
      result+="# TYPE "+meta_name+" gauge\n"
      datas.forEach(function(data){
          // ;
          //result+=data.name+'{name="'+data.name+'"} "'+data.value+" "+d.getTime()+"\n"
          result+=meta_name+'{name="'+data.name+'"} '+data.value+'\n'
      })
      res.set('Content-Type', 'text/plain');
      res.send( result );
  })
});

module.exports = router;
